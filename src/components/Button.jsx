function Button({ onClick, vairante, title }) {
  return (
    <button
      className={`p-2 ${vairante ? vairante : "bg-gray-500 "} rounded-lg`}
      onClick={onClick}
    >
      {title}
    </button>
  );
}

export default Button;
