function ImageCard({ id, image }) {
  return (
    <div className="w-full h-full" key={id}>
      <img src={image} alt={image} className="w-full h-full object-cover" />
    </div>
  );
}

export default ImageCard;
