function MiniCardImage({ onSlecet: onSelect, active, index, image }) {
  return (
    <div
      className={`w-16 h-12 rounded-lg overflow-hidden ${
        index === active ? "border-4" : ""
      } border-white`}
      onClick={onSelect}
    >
      <img src={image} alt={image} className="w-full h-full object-cover" />
    </div>
  );
}

export default MiniCardImage;
