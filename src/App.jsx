import { useState } from "react";
import "./App.css";
import { imageData } from "./utils/data.js";
import ImageCard from "./components/ImageCard";
import MiniCardImage from "./components/MiniCardImage";
import Button from "./components/Button";

function App() {
  const [active, setActive] = useState(0);

  const handleNext = () => {
    if (active + 1 === imageData.length) {
      setActive(0);
    } else {
      setActive(active + 1);
    }
  };

  const handlePrev = () => {
    if (active === 0) {
      setActive(imageData.length - 1);
    } else {
      setActive(active - 1);
    }
  };

  const handleSelecteImage = (index) => {
    setActive(index);
  };

  return (
    <div className="App">
      <div className="w-[600px] h-[500px] relative rounded-lg ">
        <ImageCard {...imageData[active]} />
        {/* <ImageCard image={imageData[active].image} id={imageData[active].id} /> */}

        <div className="absolute bottom-0 left-0 w-full h-16 flex justify-center items-center gap-4">
          {imageData.map((mon_image, index) => (
            <MiniCardImage
              key={index}
              index={index}
              active={active}
              onSelect={() => handleSelecteImage(index)}
              {...mon_image}
            />
          ))}
        </div>
      </div>

      <div className="flex gap-3">
        <Button onClick={handlePrev} title={"prev image"} />
        <Button
          vairante={"bg-blue-500"}
          onClick={handleNext}
          title={"next image"}
        />
      </div>
    </div>
  );
}

export default App;
